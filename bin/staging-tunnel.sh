#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

GITLAB_USER=${GITLAB_USER:-$USER}
ETC_HOSTS=/etc/hosts
IP="127.0.0.1"
HOSTNAME=staging.gitlab.com
BASTION_HOST=lb-bastion.gstg.gitlab.com
TUNNEL_TARGET_HOST=fe-01-lb-gstg.c.gitlab-staging-1.internal

function remove_host() {
    if [[ -n "$(grep $HOSTNAME /etc/hosts)" ]]
    then
        echo "$HOSTNAME Found in your $ETC_HOSTS, Removing now...";
        sudo sed -i".bak" "/$HOSTNAME/d" $ETC_HOSTS
    else
        echo "$HOSTNAME was not found in your $ETC_HOSTS";
    fi
}

function add_host() {
    HOSTS_LINE="$IP\t$HOSTNAME"
    if [[ -n "$(grep $HOSTNAME /etc/hosts)" ]]
    then
      echo "$HOSTNAME already exists : $(grep $HOSTNAME $ETC_HOSTS)"
    else
      echo "Adding $HOSTNAME to your $ETC_HOSTS";
      sudo -- sh -c -e "echo '$HOSTS_LINE' >> /etc/hosts";

      if [[ -n "$(grep $HOSTNAME /etc/hosts)" ]]
      then
        echo "$HOSTNAME was added succesfully \n $(grep $HOSTNAME /etc/hosts)";
      else
        echo "Failed to Add $HOSTNAME, Try again!";
      fi
    fi
}


# Make sure ssh works before
ssh ${GITLAB_USER}@${BASTION_HOST} date

sudo echo sudo obtained

echo "Logging into staging bastion as $GITLAB_USER"

ssh -v -N -L 8443:${TUNNEL_TARGET_HOST}:443 ${GITLAB_USER}@${BASTION_HOST} &

echo Running socat listener

sudo socat -d TCP4-LISTEN:443,fork TCP4-CONNECT:localhost:8443 &
SOCAT_PID=$!

function finish {
  remove_host
  echo killing ssh tunnel
  kill %1
  echo killing socat
  sudo kill $SOCAT_PID
}
trap finish EXIT


add_host

echo "Now visit https://${HOSTNAME}"
echo "Press any key to close the tunnel"
read
